package vmorales;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
//Libreria para frame
import javax.swing.JFrame;
//librerias para teclas
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.sun.opengl.util.Animator;


public class Transformacionesbasicas extends JFrame implements KeyListener{

    public Transformacionesbasicas() {
        setSize(600, 600);
        setLocation(10, 40);
        setTitle("Uso Basico de Tranformaciones");
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);        
        Animator animator = new Animator(canvas);
        animator.start();        
        addKeyListener(this);
    }

    public static void main(String args[]) {
        Transformacionesbasicas myframe = new Transformacionesbasicas();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    //Variables para instanciar funciones de opengl
    static GL gl;
    static GLU glu;

    //Variables para las transformaciones
    private static float trasladaX=0;
    private static float trasladaY=0;
    private static float escalaX=1;
    private static float escalaY=1;
    private static float rotarX=0;
    private static float rotarY=0;

    public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {
            //Inicializamos las variables
            //INICIALIZA GLU
            glu = new GLU();
            //Inicializa Gl
            gl = arg0.getGL();

            gl.glClear(gl.GL_COLOR_BUFFER_BIT);
            //Establecemos el color de fondo de la ventana de colores RGB
            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);            
            //Establecemos los parametros de proyeccion
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
            //el grosor de las lineas
            gl.glLineWidth(2f);
            //el tama�o de los puntos
            gl.glPointSize(3.0f);
            //Matriz identidad
            gl.glLoadIdentity();

            //Aplicar movimiento al cuadrado
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glScalef(escalaX, escalaY, 0);
            gl.glRotatef(rotarX, 1, 0, 0);
            gl.glRotatef(rotarY, 0, 1, 0);
            gl.glColor3f(0, 0, 1);//se le da color
            gl.glBegin(gl.GL_QUADS);//se crea el cuadrado
                gl.glVertex2f(0, 0);
                gl.glVertex2f(0.25f, 0);
                gl.glVertex2f(0.25f, 0.25f);
                gl.glVertex2f(0, 0.25f);
            gl.glEnd();
         
            gl.glFlush();
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

        //En este metodo se queda el mismo segmento que hemos venido manejando
        public void init(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }
    }

    //Metodo para el teclado
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            trasladaX+=.1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            trasladaX-=.1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_UP){
            trasladaY+=.1f;
            System.out.println("Valor en la traslacion de Y: " + trasladaY);
        }

        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            trasladaY-=.1f;
            System.out.println("Valor en la traslacion de Y: " + trasladaY);
        }

        if(e.getKeyCode() == KeyEvent.VK_D){
            escalaX+=1f;
            System.out.println("Valor en la escalacion en X: " + escalaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_A){
            escalaX-=1f;
            System.out.println("Valor en la escalacion en X: " + escalaX);
        }

         if(e.getKeyCode() == KeyEvent.VK_R){
            rotarX+=1f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_T){
            rotarX-=1f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_Y){
            rotarY+=1f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }

        if(e.getKeyCode() == KeyEvent.VK_U){
            rotarY-=1f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }

         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            trasladaX=0;
            trasladaY=0;
            escalaX=1;
            escalaY=1;
            rotarX=0;
            rotarY=0;
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }
}